package com.yinuo.face.user;


import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.blankj.utilcode.util.TimeUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yinuo.face.R;

import java.util.List;

/**
 * Created by panmingzhi on 2018/10/4.
 */

public class UserListAdapter extends BaseQuickAdapter<User,BaseViewHolder> {

    public UserListAdapter(int layoutResId, @Nullable List<User> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, User item) {
        helper.setText(R.id.user_identifier,item.getUserIdentifier());
        helper.setText(R.id.user_name, item.getUserName());
        helper.setText(R.id.user_createTime, TimeUtils.date2String(item.getCreateTime()));
        Glide.with(mContext).load(item.getUserHeader()).into((ImageView) helper.getView(R.id.user_header));
    }
}
