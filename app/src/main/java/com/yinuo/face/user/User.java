package com.yinuo.face.user;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.yinuo.face.AppDatabase;

import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by panmingzhi on 2018/10/4.
 */

@Table(database = AppDatabase.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @PrimaryKey
    private UUID id;
    @Column
    private String userName;
    @Column
    private String userIdentifier;
    @Column
    private String userHeader;
    @Column
    private Date createTime;
}
