package com.yinuo.face;

import android.app.Application;
import android.util.Log;

import com.google.common.base.Stopwatch;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.util.concurrent.TimeUnit;

/**
 * Created by panmingzhi on 2018/10/4.
 */

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Stopwatch started = Stopwatch.createStarted();

        PrettyFormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder().tag("face").showThreadInfo(false).build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));


        FlowManager.init(this);

        ArcSdkEngine.getInstance().init();
        Log.i("face", "初始化耗时:" + started.elapsed(TimeUnit.MILLISECONDS));
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        ArcSdkEngine.getInstance().unInit();
    }
}
