package com.yinuo.face;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;

import com.orhanobut.logger.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CameraPreview extends TextureView {

    private Activity activity;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    public CameraDevice cameraDevice;
    private int width = 1080;
    private int height = 1920;


    public CameraPreview(Context context) {
        super(context);
    }

    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraPreview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onResume(Activity activity) {
        this.activity = activity;
        startBackgroundThread();
        setSurfaceTextureListener();
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }


    private void openCamera(String cameraId, int width, int height) {
        this.width = width;
        this.height = height;

        Map<String, Size[]> cameraIdMap = getCameraIdMap();

        CameraManager cameraManager = (CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE);
        if (ActivityCompat.checkSelfPermission(this.activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            cameraManager.openCamera(cameraId, cameraStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Size[]> getCameraIdMap(){
        Map<String, Size[]> cameraIdMap = new HashMap<>();
        try {
            CameraManager cameraManager = (CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE);
            String[] cameraIdList = cameraManager.getCameraIdList();
            for (String cameraId : cameraIdList) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }
                Size[] outputSizes = map.getOutputSizes(ImageFormat.JPEG);
                cameraIdMap.put(cameraId, outputSizes);
            }
        } catch (CameraAccessException e) {
            Logger.e(e,"相机访问异常");
        }
        return cameraIdMap;
    }

    private CameraDevice.StateCallback cameraStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            Logger.i("相机状态己打开:%s",camera.getId());
            CameraPreview.this.cameraDevice = cameraDevice;
            startPreview(camera);
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            Logger.i("相机状态己断开:%s",camera.getId());
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            Logger.e("相机状态错误:%s",camera.getId());
        }
    };

    public void setSurfaceTextureListener() {
        super.setSurfaceTextureListener(new SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                Logger.i("预览窗口尺寸: 宽=%d 高=%d",width,height);
                openCamera("1",width,height);
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                Logger.i("预览窗口尺寸改变: 宽=%d 高=%d",width,height);
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });
    }

    private void startPreview(CameraDevice cameraDevice) {
        try {

            SurfaceTexture surfaceTexture = getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(1920,1080);
            Surface surface = new Surface(surfaceTexture);
            final CaptureRequest.Builder captureRequest = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequest.addTarget(surface);

            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    Logger.i("相机预览成功");

                    try {
                        session.setRepeatingRequest(captureRequest.build(), null, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        Logger.e(e,"相机访问异常");
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Logger.w("相机预览失败");
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            Logger.e(e,"相机访问异常");
        }

    }
}